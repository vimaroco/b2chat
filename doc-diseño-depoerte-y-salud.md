# Sistema de Recepción de Órdenes de Compra
---
## Problema a resolver
### Descripción general del requerimiento

El  comercio **Deporte y Salud** requiere el diseño y la implementacion de un módulo para la recepción de órdenes de compra desde su plataforma web. 

La orden de compra puede ser hecha por el comprador en la plataforma web, una vez haya seleccionado los productos que desea comprar junto con la cantidad de cada uno. Adicionalmente, para realizar la orden es necesario tener la dirección de entrega de los productos y los datos del comprador tales como: nombre, número de identificación y los demas que se consideren necesarios . En la actualidad el comercio solo puede hacer entregas en Colombia y recibir pagos contra entrega.

Este módulo debe exponer los servicios necesarios para que el comprador del comercio pueda realizar la orden de compra. La parte visual de la plataforma web será realizada por otro equipo y hará uso de los servicios expuestos.

Una vez recibida la orden de compra, al comprador se le debe enviar una notificación via WhatsApp o email con la información de la orden y la fecha estimada de entrega.

### Otros requerimientos

- Debe proveerse un servicio para la consulta de las órdenes que deben entregarse en una fecha específica.
- Se espera que la cantidad de órdenes recibidas por este módulo crezca bastante, por lo que es necesario pensar en los mecanismos necesarios para poder soportar este crecimiento.
- También es necesario considerar que el comercio requiere alta disponibilidad de este módulo para no perder las órdenes realizadas por los compradores. Es decir, el módulo debe ser tan resistente como se pueda a fallas de hardware o software. De ocurrir alguna de estas, el módulo debería seguir operando y perder la menor cantidad de información posible.

### Entregables

- Documento con las preguntas que se consideren adecuadas para un mejor entendimiento de los requerimientos. Debe preguntarse por las cosas que se consideren ambiguas o donde sea necesaria mas información.
- Diseño arquitectónico de la solución.

### Preguntas para un mejor entendimiento de la solución
Se deben resolver los siguientes asuntos para determinar si casos de usos que están fuera de alcance, entran a este:
* Se cuenta con un servicio de logística que gestione el flujo requerido para entregar los produtos?
* El listdos de productos se obtendrá de un servicio del sistema de administracion de inventario o se tendrá un espejo del listado de productos propio?
* Se el se cuenta con disponibilidad de un servicio de administración de inventario de productos?
* Cuál es el crecimiento estimado de usuarios? (necesario para determinar el costo de los recursos necesario para soportarlos).
* Cual es la estimación de compradores diarios, picos máximos y mínimos, actual y proyectados? en qué horas?.
* Se cuenta con un servicio de administración de usuarios global y se espero que el servicio tenga su base de datos propia de usuarios. 


### Alcance
El alcance de este proyecto el desarrollo de un micro servicio que permita la recepción de ordenes de compra, notifique, reporte fechas de envío.

#### Casos de uso

* Como comprador requiero hecer una selección de productos y la cantidad de estos que deseo comprar en la plataforma web.
* Como comprador requiero hacer una orden de compra con los productos seleccionados en la plataforma web.
* Como comprador requiero ingresar los datos necesarios para entrega de los productos.
* Como comprador requiero recibir una notificación con la información de la orden y la fecha estimada de la entrega.
* Como comprador requiero poder seleccionar el medio por el cual recibir la notificación WhatsApp o Email.
* Como usuario administrador requiero comsultar las ordenes que deben entregase en una fecha específica.

#### Casos de uso fuera de alcance
* Como administrador requiero que el sistema haga seguimiento del estado del envío al usuario.
* Como usuario requiero ver el número disponible de un producto.

---
## Arquitectura
### Estilo arquitectónico
Se opta por el desarrollo de un componente independiente, el cual deberá ser orquiestado dentro del resto de servicios de la compañia.
Para esto hará invocación implicita los casos que requieran orquestación, detallados mas adelante, e invocación explicita para casos muy específicos en los que el servicio actué como respositorio de consulta de informacion requerida para el funcionamiento del servicio.

![Estilo arquitectónico](estilo-artquitectonico.png)

### Diagramas
#### Diagrama de componentes
![Diagrama de componentes](diagrama-de-componentes.png)

### Modelo de datos
Las entidades del dominio son:

- Producto
{
    idProducto,
    Nombre,
    Cantidad,
}
- Orden
{
    IdOrden,
    IdCliente,
    NombreCompleto,
    FechaOrden,
    FechaEstimadaEntrega,
    FechaEntrega,
    Productos[]
}

---
## Plan de pruebas
* Crear un proyecto de pruebas que valide los siguientes casos de uso:
Para este servicio se hará una prueba end to end
- Seleccionar un set de productos con sus respecticas cantidades - Enviar orden de compra - simular que se envía na notificacion - consultar la orden de compra especifica.
- Se creará un proyecto que valide el volumen requerido de usuarios.
    - Se harán pruebas de stres con un set de productos enviando ordenes de compras, validanr notificaciones - validar consutal de ordenes de compra.
    - Se provocarán errores de time out para validar la recuperación ante fallos.
 
---
## Integración continua
Plan de despliegue e integración continua:

![Plan de despliegue e integrción continua](plan-de-distribucion-feature.png)



---
## Limitaciones
* La llamada al API para obterne el listado de producto no debe exeder los mímites de la tencia de 500ms.
* La llamada al servicio de recepción de ordenes de compras debe tener una latencia de respuesta menor a 100ms.
* La llamada al servicio de consulta de ordenes a entregar debe tener una latencia de respuesta menor a 500ms. 
---
## Costo
Descripción/Análisis de costos
Estos costos se harán basados en las respuesta a las preguntas de volumen de usuarios diarios y proyección de crecimiento, dados estos valores se hará las estimación.
